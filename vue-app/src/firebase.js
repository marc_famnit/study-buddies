import * as firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'

const firebaseConfig = {
	apiKey: "AIzaSyCXoct_gug1cLgpXYDXDBgQyYApvdQ7vlM",
	authDomain: "sys-iii-study-buddies.firebaseapp.com",
	databaseURL: "https://sys-iii-study-buddies.firebaseio.com",
	projectId: "sys-iii-study-buddies",
	storageBucket: "sys-iii-study-buddies.appspot.com",
	messagingSenderId: "1083349082032",
	appId: "1:1083349082032:web:e2dab35ae1558f04438075"
  };
firebase.initializeApp(firebaseConfig)

const auth = firebase.auth()
const firestore = firebase.firestore()
export {
	auth,
	firestore
}