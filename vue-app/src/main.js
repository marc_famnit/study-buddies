import Vue from 'vue'
import { auth } from './firebase'
import VueRouter from 'vue-router'
import store from './store'
import routes from './routes'
import vuetify from './plugins/vuetify'
import VueClipBoard from 'vue-clipboard2'
import App from './App.vue'

Vue.config.productionTip = false
Vue.use(VueRouter)
Vue.use(VueClipBoard)

const router = new VueRouter({
	mode: 'history',
	routes,
	scrollBehavior() {
        return {x: 0, y: 0}
    }
})

router.beforeEach((to, from, next) => {
	if (to.matched.some(x => x.meta.login)) {
		auth.onAuthStateChanged(user => {
			if (user) {
				next('/')
			} else {
				next()
			}
		})
	} else {
		auth.onAuthStateChanged(user => {
			if (user) {
				next()
			} else {
				next('/login')
			}
		})
	}
})

new Vue({
	vuetify,
	router,
	store,
	render: h => h(App)
}).$mount('#app')
