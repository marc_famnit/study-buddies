import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import colours from 'vuetify/lib/util/colors'

Vue.use(Vuetify)

export default new Vuetify({
	theme: {
		themes: {
			light: {
				primary: colours.cyan
			}
		}
	}
})
