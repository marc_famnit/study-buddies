import Home from './components/Home.vue'
import NewMeetUp from './components/NewMeetUp.vue'
import EditMeetUp from './components/EditMeetUp.vue'
import MyMeetUps from './components/MyMeetUps.vue'
import JoinedMeetUps from './components/JoinedMeetUps.vue'
import Tutors from './components/Tutors.vue'
import Settings from './components/Settings.vue'
import Login from './components/Login.vue'

const routes = [
	{ path: '/', component: Home },
	{ path: '/newmeetup', component: NewMeetUp },
	{ path: '/editmeetup/:m_id', component: EditMeetUp },
	{ path: '/mymeetups', component: MyMeetUps },
	{ path: '/joinedmeetups', component: JoinedMeetUps },
	{ path: '/tutors', component: Tutors },
	{ path: '/settings', component: Settings },
	{ path: '*', redirect: '/' },
	{ path: '/login', component: Login, meta: { login: true } },
]

export default routes