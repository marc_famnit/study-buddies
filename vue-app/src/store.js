import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

const store = new Vuex.Store({
	plugins: [createPersistedState({
        storage: window.sessionStorage,
    })],
	state: {
		autoSubjectFilter: true,
		autoDateFilter: false,
	},
	getters: {
		autoSubjectFilter(state) {
			return state.autoSubjectFilter
		},
		autoDateFilter(state) {
			return state.autoDateFilter
		},
	},
	mutations: {
		setAutoSubjectFilter(state, val) {
			state.autoSubjectFilter = val
		},
		setAutoDateFilter(state, val) {
			state.autoDateFilter = val
		},
	}
})

export default store